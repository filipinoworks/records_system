<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_records")
 */
class Records
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email_address;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100)
    */
    private $gender;

    /**
     * @ORM\Column(type="text")
     */
    private $description;


    /** @ORM\Column(name="`isDeleted`", type="integer") */
    private $isDeleted;


    public function setName($name) {
    	$this->name = $name;
    }
    public function setEmailAddress($email_address) {
    	$this->email_address = $email_address;
    }
    public function setPassword($password) {
    	$this->password = $password;
    }
    public function setGender($gender) {
    	$this->gender = $gender;
    }
    public function setDescription($description) {
    	$this->description = $description;
    }
    public function setIsDeleted($deleted) {
        $this->isDeleted = $deleted;
    }
}
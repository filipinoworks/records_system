<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\Records;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends Controller
{	
	/**
     * @Route("/", name="homepage")
    */
    public function index()
    {
       	return $this->render('pages/homePage.html.twig');
    }

    /**
     * @Route("/welcome", name="welcomePage")
    */
    public function welcome()
    {
       	return $this->render('pages/welcome.html.twig');
    }
    /**
     * @Route("/records/showAddPage", name="showAddPage")
    */
    public function showAddPage()
    {	

		$form = $this->createFormBuilder()
		    ->add('name', TextType::class,array(
		        'attr' => array(
		             'placeholder' => 'Name',
		             'id' => ''
		        )))
		    ->add('email_address', EmailType::class,array(
		        'attr' => array(
		             'placeholder' => 'Email Address',
		             'id' => ''
		        )))
		    ->add('password', PasswordType::class,array(
		        'attr' => array(
		             'placeholder' => 'Password',
		             'id' => ''
		        )))
		    ->add('gender', ChoiceType::class, array(
			    'choices'  => array(
			        'Male' => null,
			        'Female' => true
			    ),
			    'attr' => array(
		             'id' => ''
		        )))
		    ->add('description', TextareaType::class,array(
		        'attr' => array(
		             'placeholder' => 'Description',
		             'id' => ''
		        )))
		    ->getForm();

        return $this->render('pages/newRecord.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/records/save", name="saveNewRecord")
    */
    public function save(request $request)
    {	
    	$data =  $request->getContent();
    	$json = json_decode($data, true);
    	$request = $json;
    	$manager = $this->getDoctrine()->getManager();

    	if(isset($request['id']) && $request['id'] != "") {
    		$records = $manager->getRepository(Records::class)->find($request['id']);
    	}else{
    		$records = new Records();
    	}
    	
    	$request["gender"] = $request["gender"] == "0" ? "male" : "female";

	    $records->setName($request["name"]);
	    $records->setPassword($request["password"]);
	    $records->setEmailAddress($request["email_address"]);
	    $records->setGender($request["gender"]);
	    $records->setDescription($request["description"]);
	    $records->setIsDeleted(0);
	    $manager->persist($records);
	    $manager->flush();
    		
    	$message = array();
    	$message["status"] = true;
    	$message["message"] = "Record has been successfully saved!.";
    	$message = json_encode($message);
    	return new Response($message);
    }

    /**
     * @Route("/records/delete", name="deleteRecord")
    */
    public function delete(request $request)
    {	

    	$data =  $request->getContent();
    	$json = json_decode($data, true);
    	$request = $json;

    	$manager = $this->getDoctrine()->getManager();
	    $records = $manager->getRepository(Records::class)->find($request['id']);
	    $records->setIsDeleted(1);
	    $manager->persist($records);
	    $manager->flush();
    		
    	$message = array();
    	$message["status"] = true;
    	$message["message"] = "Record has been successfully removed!.";
    	$message = json_encode($message);
    	return new Response($message);
    }

    /**
     * @Route("/records/list", name="list")
    */
    public function listData()
    {	
    	$response = array();
		$repo = $this->getDoctrine()->getRepository(Records::class);
		$records = $repo->createQueryBuilder('r')->andWhere('r.isDeleted = 0')->getQuery()->getArrayResult();
		$response["data"] = $records;

		//var_dump($response);
		return new Response(json_encode($response));
    }

    /**
     * @Route("/records/listPage", name="listPage")
    */
    public function listPage()
    {	
		return $this->render('pages/listPage.html.twig');
    }

    /**
     * @Route("/records/modal", name="modalPage")
    */
    public function modalWrapper()
    {	
		return $this->render('pages/modalWrapper.html.twig');
    }
}

var app = angular.module('app', ['ngRoute']);




/*SERVICES*/
app.service('RecordsService', function($http, $q) {

	var pipe = {};
    pipe.save = function(data) {
    	var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : "/records/save",
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}

	pipe.list = function() {

		var deferred = $q.defer();

		$http({
	        method : "GET",
	        url : "/records/list",
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}

	pipe.delete = function(data) {

		var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);

		$http({
	        method : "POST",
	        url : "/records/delete",
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}

	pipe.arrayToParams = function($array) {
		var postParam = {};
		for (key in $array) {
	        postParam[key] = $array[key]
	    }
		return postParam;
	}
	return pipe;
}); 



/*CONTROLLER*/
app.controller('recordsController', function($scope, RecordsService, $compile, $route) {
	$scope.formData = {};
	$scope.records = [];
	$scope.temp = [];
	$scope.datatable = {};
	$scope.recordForm = {};
	$scope.busy = false;
	$scope.error = {};
	$scope.error.status = "Warning";
	$scope.error.message = "Some field are required and invalid. please try again.";
	$scope.state_value = "";


	//$scope.state = $route.current.$$route.route_name;

	/*RecordsService.list().then(function(response) {
		$scope.records = response;
	});*/
	$scope.saveNewRecord = function() {
		console.log($scope.recordForm)
		$scope.busy = true;
		RecordsService.save($scope.formData).then(function(response) {

			if($scope.formData['id'] != "" && $scope.formData['id'] != undefined) {
				$.notify("Record has been updated successfully!", "success");
			}else{
				$.notify("Record has been saved successfully!", "success");
			}
			
			$('#records_datatable').DataTable().ajax.reload();
			$('#quickRecordModal').modal('hide');
			$('.spinner').remove();
			
			$scope.busy = false;
			$scope.formData = {};
			$scope.$apply();
			
		});
	}
	$scope.submitRecords = function(isValid) {
		if (isValid) {
	      	$scope.saveNewRecord();
	    } else {
	    	
	    }
	}
	$scope.removeRow = function(id,e) {
		$scope.customLoader(e)
		var data = {"id" : id};
		$scope.busy = true;
		RecordsService.delete(data).then(function(response) {

			$.notify("Record has been removed successfully!", "success");
			$('#removeModal').modal('hide');
			$('#records_datatable').DataTable().ajax.reload();
			$('.spinner').remove();
			$scope.busy = false;
		});
	}
	$scope.refreshFormData = function() {
		$compile($('#quickRecordModal'))($scope);
		$scope.formData = {};
	}
	$scope.submitStatus = function() {
		if($scope.formData.id != undefined) {
			return "Update";
		}else{
			return "Save";
		}
	}
	$scope.customLoader = function(e) {
		var loader = "<img src ='/images/spinner.svg' class='spinner' style='height: 30px;'>";
		console.log(e)
		$(e.currentTarget).append(loader);
	}
	$scope.compileQuickModal = function() {
		var $new_wrapper = $('#form-wrapper').html()
		$('#form-wrapper').contents().remove();
		$('#form-wrapper').html($new_wrapper);
		$compile($('#form-wrapper'))($scope);
		console.log($scope.recordForm)
	}
});


/*ROUTER*/
app.config(function($routeProvider) {
    $routeProvider
    	.when('/', {
            templateUrl : '/welcome',
            controller  : 'recordsController',
            route_name  : 'Home Page'
        })
        .when('/records/showAddPage', {
            templateUrl : '/records/showAddPage',
            controller  : 'recordsController',
            route_name  : 'Add New Record'
        })
        .when('/records/listPage', {
            templateUrl : '/records/listPage',
            controller  : 'recordsController',
            route_name  : 'Listing Page'
        })
});



/*DIRECTIVES*/
app.directive('datatable', function ($compile) {
    return {
        link: function ($scope, element, attrs) {

            $scope.datatable = $(element).DataTable({
		        "bServerSide" :false,
		        "serverSide": true,
		        "ajax": {
		            "url": "/records/list",
		            "type": "POST"
		        },
		        "createdRow": function ( row, data, index ) {
		            $compile(row)($scope);
		        },
		        "columns": [
		            { "data": "name" },
		            { "data": "password" },
		            { "data": "email_address" },
		            { "data": "gender" },
		            { "data": "description" },
				    {
		                sortable: false,
		                 	"render": function ( data, type, full, meta ) {
		                    return '<button edit-row data-email_address="'+full.email_address+'" data-description="'+full.description+'" data-password="'+full.password+'" data-gender="'+full.gender+'" data-name="'+full.name+'" data-id="'+full.id+'" class="btn btn-success" role="button">Edit</button>';
		                 }
		            },
				    {
		                sortable: false,
		                 	"render": function ( data, type, full, meta ) {
		                    return '<button delete-row data-email_address="'+full.email_address+'" data-description="'+full.description+'" data-password="'+full.password+'" data-gender="'+full.gender+'" data-name="'+full.name+'" data-id="'+full.id+'" class="btn btn-danger" role="button">Remove</button>';
		                 }
		            }
		        ]
		       
		    });
        }
    };
});


app.directive('editRow', function ($compile,$timeout) {
    return {
        link: function ($scope, element, attrs) {
        	$(element).click(function() {

        		var $new_modal = $('#quickRecordModal').html()
        		$('#quickRecordModal').contents().remove();
        		$('#quickRecordModal').html($new_modal);
        		$compile($('#quickRecordModal'))($scope);
        		$('#quickRecordModal').modal('show');

	            $scope.$apply(function() { 
					$scope.formData.name = attrs.name;
	        		$scope.formData.id = attrs.id;
	        		$scope.formData.password = attrs.password;
	        		$scope.formData.email_address = attrs.emailAddress;
	        		$scope.formData.description = attrs.description;
	        		$scope.formData.gender = attrs.gender == "male" ? 0 : 1;
        		});
        	});
        }
    };
});

app.directive('deleteRow', function ($compile,$timeout) {
    return {
        link: function ($scope, element, attrs) {
        	$(element).click(function() {
        			
        		var $new_modal = $('#removeModal').html()
        		$('#removeModal').contents().remove();
        		$('#removeModal').html($new_modal);
        		$compile($('#removeModal'))($scope);
        		$('#removeModal').modal('show');
        	
        		$scope.$apply(function() { 
					$scope.temp.name = attrs.name;
	        		$scope.temp.id = attrs.id;
        		});

        		console.log(attrs.name)
        	});
        }
    };
});

app.directive('loaderIcon', function ($compile,$timeout) {
    return {
        link: function ($scope, element, attrs) {
        	$(element).click(function(e) {
				var loader = "<img src ='/images/spinner.svg' class='spinner' style='height: 30px;'>";
				$(e.currentTarget).append(loader);
        	});
        }
    };
});
